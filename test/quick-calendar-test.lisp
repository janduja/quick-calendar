(defpackage quick-calendar-test
  (:use :common-lisp :lisp-unit :quick-calendar))

(in-package :quick-calendar-test)

(defparameter work-calendar-file-path 
  (make-pathname :directory (pathname-directory *load-pathname*)
                 :name "work-calendar"
                 :type nil))

(define-test load-file-test
  (let ((working-calendar (load-to-lined-list work-calendar-file-path)))
    (assert-equal 4 (length working-calendar))
    (assert-equal 2 (car (nth 0 working-calendar)))
    (assert-equal "2017-06-23 09:00 +01:00 2h10m | Meet Filippo in Palermo" (cadr (nth 0 working-calendar)))
    (assert-equal 3 (car (nth 1 working-calendar)))
    (assert-equal "2017-06-17 11:00 +00:00 15m | Test new features" (cadr (nth 1 working-calendar)))
    (assert-equal 4 (car (nth 2 working-calendar)))
    (assert-equal "2017-06-24 09:00 +02:00 5d | Meet Svetlana in Kiev" (cadr (nth 2 working-calendar)))
    (assert-equal 7 (car (nth 3 working-calendar)))
    (assert-equal "d 2017-06-19 2017-11-19 17:30 +00:00 5m | Log time" (cadr (nth 3 working-calendar)))))

(setq *print-failures* t)
(run-tests :all)
