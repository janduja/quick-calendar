(in-package :asdf)

(defsystem :quick-calendar
  :description "Utility to show the calendar events from simple text files"
  :version "1.0"
  :author "Raffaele Arecchi <raffaele@janduja.com>"
  :license "MIT"
  :components
  ((:file "quick-calendar")))