(defpackage quick-calendar
  (:use :common-lisp)
  (:export :load-to-lined-list))

(in-package :quick-calendar)

(defun str-starts-with (x y)
  "Returns T if the first string starts with the second string"
  (string= y (subseq x 0 (length y))))

(defun load-to-lined-list (file-path)
  "Reads a file from the input path, removing the commented lines starting with '#', then returns
a new list of couples (line-number string-at-that-line)."
  (let ((result nil))
    (with-open-file (stream file-path)
      (do ((line (read-line stream nil) (read-line stream nil))
           (line-number 1 (+ 1 line-number)))
          ((null line))
        (let ((trimmed-line (string-trim " " line)))
          (if (and (not (string= "" trimmed-line))
                   (not (str-starts-with trimmed-line "#")))
              (setq result (cons (list line-number trimmed-line) result))))))
     (reverse result)))


;;(defun encode-single-event (event-string)
;;)
;;
;;(defun encode-recurrent-event (event-string)
;;)
;;
;;(defun encode (event-line)
;;  (let ((line-number (car event-line))
;;        (event-string (cdar event-line)))
;;    (if (digit-char-p (char event-string 0))
;;        (encode-single-event event-string)
;;        (encode-recurrent-event event-string))))

